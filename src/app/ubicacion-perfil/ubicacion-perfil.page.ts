﻿import { Component, OnInit } from '@angular/core';
import { MenuController, NavController, ModalController, ToastController } from '@ionic/angular';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import * as Leaflet from 'leaflet';
import 'leaflet-routing-machine';
import { DetalleUserComponent } from '../components/detalle-user/detalle-user.component';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
//import 'leaflet-easybutton';
declare var L: any;

@Component({
  selector: 'app-ubicacion-perfil',
  templateUrl: './ubicacion-perfil.page.html',
  styleUrls: ['./ubicacion-perfil.page.scss'],
})
export class UbicacionPerfilPage implements OnInit {
  map: Leaflet.Map;
  mi_lat;
  mi_lon;
  Latitud;
  Longitud;
  apodo;
  foto;
  municipio;
  distan;
  distanc;
  info = false;
  //npm install leaflet-routing-machine
  //https://www.liedman.net/leaflet-routing-machine/

  //edit-config (config.xml)
  //<application android:networkSecurityConfig="@xml/network_security_config" /> 
  constructor(private menu: MenuController, private navCtrl: NavController, private screen: ScreenOrientation, private toastController: ToastController,
    private detUser: DetalleUserComponent, private modalController: ModalController, private socialSharing: SocialSharing) {
    this.screen.lock(this.screen.ORIENTATIONS.PORTRAIT).catch(function(error) {});  this.screen.onChange().subscribe(() => { });
    this.menu.enable(false);
    this.datosUbicacion();
  }
  
  ngOnInit() {
  }

  
  fotoModal(fot, apod) {
    this.detUser.cargarFoto(fot, apod);
    this.openModal(fot, apod);
  }

  async openModal(val, apod) {
    const modal = await this.modalController.create({
      component: DetalleUserComponent,
      cssClass: 'my-custom-modal-css',
      componentProps: {
        'foto': val,
        'apodo': apod
      }
    });
    return await modal.present();
  }
  
  aqui = "Aquí estoy";
  ionViewWillEnter() {
    setTimeout(() => {
      this.map = new Leaflet.Map('mapId').setView([this.mi_lat, this.mi_lon], 11.3);
      Leaflet.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png').addTo(this.map);  //osm  //openstreetmap--------http://{s}.tile.osm.org/{z}/{x}/{y}.png------'http://{s}.tile.osm.org/{z}/{x}/{y}.png'
                           //'//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
      var greenIcon = Leaflet.icon({
        iconUrl: '/assets/icon/marcador.png'
      });

      Leaflet.marker([this.mi_lat, this.mi_lon], { icon: greenIcon }).addTo(this.map).bindPopup(this.aqui, {autoClose: false, autoPan: false}).openPopup();
      Leaflet.marker([this.Latitud, this.Longitud], { icon: greenIcon }).addTo(this.map).bindPopup(this.apodo, {autoClose: false, autoPan: false}).openPopup();
      this.map.setMaxBounds([[this.mi_lat, this.mi_lon], [this.Latitud, this.Longitud]]);

      L.Routing.control({
        waypoints: [
          L.latLng(this.mi_lat, this.mi_lon),
          L.latLng(this.Latitud, this.Longitud)
        ],
        collapsible: false,
        show: false,
      }).addTo(this.map);
      this.info = true;
    }, 1200);
  }

  share(){
    this.socialSharing.share('', 'App Citas', null, 'Citamed disponible en tiendas').then(()=>{
    }).catch(() => {
    });
  }

  datosUbicacion() {
    const d = localStorage.getItem('mi_ubic');
    var ubi = d.split(",");
    this.mi_lat = ubi[0];
    this.mi_lon = ubi[1];

    const p_u = localStorage.getItem('perfil_ubic');
    var u = p_u.split(",");
    this.Latitud = u[0];
    this.Longitud = u[1];
    this.apodo = u[2];
    this.foto = u[3];
    this.municipio = u[4];
  }

  myToast: any;
  addFavorit() {
    this.myToast = this.toastController.create({
      message: 'Agregado a favoritos',
      position: 'bottom',
      duration: 1150
    }).then((toastData) => {
      toastData.present();
    });
  }

  back() {
    this.navCtrl.navigateForward('/perfil');
    localStorage.removeItem('perfil_ubic');
  }

}
