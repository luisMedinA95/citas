import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UbicacionPerfilPage } from './ubicacion-perfil.page';

const routes: Routes = [
  {
    path: '',
    component: UbicacionPerfilPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UbicacionPerfilPageRoutingModule {}
