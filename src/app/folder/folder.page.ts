import { Component, OnInit, Input, ViewChild  } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MenuController, PopoverController, ToastController, ActionSheetController, NavController, LoadingController, ModalController, NavParams } from '@ionic/angular';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { PopoverPage } from '../popover/popover.page';
import { ServicioService } from '../services/servicio.service';
import { AppComponent } from '../app.component';
import { DetalleUserComponent } from '../components/detalle-user/detalle-user.component';

@Component({
  selector: 'app-folder',
  templateUrl: './folder.page.html',
  styleUrls: ['./folder.page.scss'],
})
export class FolderPage implements OnInit {
  public folder: string;
  perfiles = [];
  mis_favoritos = [];
  mis_fav_aux = [];
  favoritos_actualizados = [];
  favoritos_actualizados_aux = [];
  showList = false;
  private items = [];
  dato;
  oculto = false;

  estado_activo = '1';
  constructor(private activatedRoute: ActivatedRoute, private menu: MenuController, private screen: ScreenOrientation, private navCtrl: NavController,
    private popoverController: PopoverController, private toastController: ToastController, private actionSheetController: ActionSheetController,
    private service: ServicioService, private loadingController: LoadingController, private apC: AppComponent, private modalController: ModalController, private detUser: DetalleUserComponent) {
    this.screen.lock(this.screen.ORIENTATIONS.PORTRAIT).catch(function(error) {}); this.screen.onChange().subscribe(() => { });
    this.perfiles = this.service.perfiles_provider;
    this.apC.consultarUsuario();
    this.menu.enable(true);
    this.initializeItems();
  }
  
  ngOnInit() {
  }

  fotoModal(fot, apod) {
    this.detUser.cargarFoto(fot, apod);
    this.openModal(fot, apod);
  }

  async openModal(val, apod) {
    const modal = await this.modalController.create({
      component: DetalleUserComponent,
      cssClass: 'my-custom-modal-css',
      componentProps: {
        'foto': val,
        'apodo': apod
      }
    });
    return await modal.present();
  }

  cambio() {
    this.apC.cambio();
  }

  initializeItems() {
    this.items = this.perfiles;
  }

  getItems(ev: any) {
    this.initializeItems();
    let val = ev.target.value;
    if (val && val.trim() != '') {
      this.showList = false;
      this.items = this.items.filter((item) => {
        this.showList = true;
        return (item.nombre.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    } else {
      this.showList = false;
    }
  }

  seleccion(val) {
    this.loading();
    setTimeout(() => {
      this.showList = false;
      let act_resultado = '1';
      localStorage.setItem('perfil_resultado', JSON.stringify(val));
      localStorage.setItem('resultado_user', act_resultado);
      this.navCtrl.navigateForward("/perfil");
      this.dato = null;
    }, 1500);
  }

  isLoading = false;
  async loading() {
    this.isLoading = true;
    return await this.loadingController.create({
      message: 'Cargando',
      duration: 2000
    }).then(a => {
      a.present().then(() => {
        console.log('loading presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort laoding'));
        }
      });
    });
  }


  async opciones(ev) {
    const popover = await this.popoverController.create({
      component: PopoverPage,
      ///cssClass: 'my-custom-class',
      event: ev,
    });
    await popover.present();
    const { data } = await popover.onDidDismiss();
    //console.log("-->: ", data.op.id_opcion);
    /*if (data.op.id_opcion === '1') {
      console.log("1");
    } else if (data.op.id_opcion === '2') {
      console.log("2");
    }else{
      console.log("else");
    } */
    console.log("Recivo: ", data);
  }

  async opc_Perfil(val) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Perfil',
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Visitar perfil',
        role: '',
        icon: 'man',
        handler: () => {
          this.loading();
          setTimeout(() => {
            console.log("Valores: ", val);
            localStorage.setItem('perfil_datos', JSON.stringify(val));
            this.navCtrl.navigateForward("/perfil");
          }, 1500);
        }
      },
      {
        text: 'Quitar',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
        }
      }, {
        text: 'Favorito',
        icon: 'heart',
        handler: () => {
          const upd_fav = localStorage.getItem('update_favoritos');
          const id_up = localStorage.getItem('Identif');
          if (upd_fav === null) {
            if (id_up === '1') {
              const v = localStorage.getItem('val_favoritos');
              this.favoritos_actualizados = JSON.parse(v);
              this.favoritos_actualizados.push(val);

              this.favoritos_actualizados_aux = this.favoritos_actualizados.filter((valorActual, indiceActual, arreglo) => {
                return arreglo.findIndex(valorDelArreglo => JSON.stringify(valorDelArreglo) === JSON.stringify(valorActual)) === indiceActual
              });

              this.showToast();
              localStorage.setItem('val_favoritos', JSON.stringify(this.favoritos_actualizados_aux));
            } else {
              this.mis_favoritos.push(val);
              this.mis_fav_aux = this.mis_favoritos.filter((valorActual, indiceActual, arreglo) => {
                return arreglo.findIndex(valorDelArreglo => JSON.stringify(valorDelArreglo) === JSON.stringify(valorActual)) === indiceActual
              });

              this.showToast();
              localStorage.setItem('val_favoritos', JSON.stringify(this.mis_fav_aux));
            }
          }
        }
      }, {
        text: 'Cancelar',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }
  myToast: any;

  showToast() {
    this.myToast = this.toastController.create({
      message: 'Agregado a favoritos',
      position: 'bottom',
      duration: 1150
    }).then((toastData) => {
      console.log(toastData);
      toastData.present();
    });
  }
}
