import { Component, OnInit, Input } from '@angular/core';
import { MenuController, NavController, ModalController, LoadingController, AlertController } from '@ionic/angular';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { Storage } from '@ionic/storage';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import swal from 'sweetalert';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.page.html',
  styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit {
  @Input() texto = 'Cargando...';
  loader = false;
  user;
  constructor(private menu: MenuController, private navCtrl: NavController, private screen: ScreenOrientation, private http: HttpClient,
    private storage: Storage, private camera: Camera, private loadingController: LoadingController, private alertCtrl: AlertController) {
    this.screen.lock(this.screen.ORIENTATIONS.PORTRAIT).catch(function(error) {}); this.screen.onChange().subscribe(() => { });
    this.menu.enable(false);
    this.accesoPerfil();
  }
  type: string;

  ngOnInit() {
    this.type = 'fot';
    /*const url = 'http://localhost:3000/allUsers';
    return this.http.get(url)
    .subscribe(subscribe => {
      console.log("->",subscribe);
    })*/
  }

  segmentChanged(ev: any) {
  }

  chats(v, f) {
    let a = '1';
    if (a === '1') {
      setTimeout(() => {
        swal("Warning", "No es posible hasta subscribirse", "warning");
      }, 600);
    } else {
      let info = v + "," + f;
      this.loading();
      setTimeout(() => {
        this.navCtrl.navigateForward('/chats');
        localStorage.setItem('chat_usr', info);
      }, 1500);
    }
  }
  ubicacion(l, lo, a, i, m) {
    if (localStorage.getItem('mi_ubic') === null || localStorage.getItem('mi_ubic') === undefined) {
      setTimeout(() => {
        swal("Error", "No es posible ubicar por el momento", "error");
      }, 600);
    } else {
      let ub = l + "," + lo + "," + a + "," + i + "," + m;
      this.loading();
      this.navCtrl.navigateForward('/ubicacion-perfil');
      localStorage.setItem('perfil_ubic', ub);
    }
  }

  isLoading = false;
  async loading() {
    this.isLoading = true;
    return await this.loadingController.create({
      message: 'Cargando',
      duration: 2000
    }).then(a => {
      a.present().then(() => {
        console.log('loading presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort laoding'));
        }
      });
    });
  }

  photoUser(val){
    console.log("V: ",val);
    this.presentAlert(val);
  }

  async presentAlert(val) {
    const alert = await this.alertCtrl.create({
      message: `<img src="${val}">`,
      cssClass: 'alertCancel'
    });
    await alert.present();
  }

  accesoPerfil() {
    let act_result = localStorage.getItem('resultado_user');
    let act_favorit = localStorage.getItem('favorito_user');
    if (act_result === '1') {
      console.log("Desde resultados");
      this.user = JSON.parse(localStorage.getItem('perfil_resultado'));
    } else {
      if (act_favorit === '2') {
        console.log("Desde favorito");
        this.user = JSON.parse(localStorage.getItem('favorito_perfil'));
      } else {
        console.log("Desde else favoritos a opciones");
        this.user = JSON.parse(localStorage.getItem('perfil_datos'));
      }
    }
  }
  back() {
    this.menu.enable(true);
    if (localStorage.getItem('favorito_user') === '2') {
      console.log(" atras de favoritos");
      this.navCtrl.navigateForward('/favoritos');
      localStorage.removeItem('favorito_perfil');
      localStorage.removeItem('favorito_user');
    } else {
      this.loader = true;
      setTimeout(() => {
        this.loader = false;
        this.texto = 'Cargando...';
        this.navCtrl.navigateForward('/folder/Inbox');
        localStorage.removeItem('perfil_datos');
        localStorage.removeItem('perfil_resultado');
        localStorage.removeItem('resultado_user');
        localStorage.removeItem('favorito_perfil');
        localStorage.removeItem('favorito_user');
        this.user = null;
      }, 1000);
    }
  }
}

