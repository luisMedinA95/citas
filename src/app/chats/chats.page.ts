import { Component, OnInit, Input } from '@angular/core';
import { NavController, NavParams, ModalController } from '@ionic/angular';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';

@Component({
  selector: 'app-chats',
  templateUrl: './chats.page.html',
  styleUrls: ['./chats.page.scss'],
})
export class ChatsPage implements OnInit {
  @Input() texto = 'Cargando...';
  loader = false;
  usuario_chat;
  foto;
  public navParams = new NavParams();
  constructor(private navCtrl: NavController, private modalController: ModalController, public nav: NavController, private screen: ScreenOrientation) { 
  this.screen.lock(this.screen.ORIENTATIONS.PORTRAIT).catch(function(error) {}); this.screen.onChange().subscribe(() => { });
   const d  = localStorage.getItem('chat_usr');
   var nom = d.split(",");
   this.usuario_chat = nom[0];
   this.foto =  nom[1];
  /* var expresion = /http([^"'\s]+)/g
    const url = d.match(expresion);
   console.log(d, url[0]);*/
  }

  ngOnInit() {
  }
  back(){
    this.loader = true;
    setTimeout(() => {
      this.loader = false;
      this.texto = 'Cargando...';
      this.navCtrl.navigateForward('/perfil');
    }, 500);
  }

}
