import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { MenuController, NavController, LoadingController, AlertController } from '@ionic/angular';
import { ServicioService } from '../services/servicio.service';
import swal from 'sweetalert';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {
  @Input() texto = 'Cargando...';
  loader = false;
  inicio = true;
  login_registro = false;

  login_user = {
    correo: '',
    password: ''
  }

  registro_user = {
    nombre: '',
    apellido: '',
    correo: '',
    password: ''
  }
  confirmar;
  em_err = false;
  usr_err = false;
  cred_err = false;
  em_error = false;
  pass_error = false;
  r_error = false;
  user_exist = false;
  expresion = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
  constructor(private screen: ScreenOrientation, private menu: MenuController, private navCtrl: NavController,
    private service: ServicioService, private loadingController: LoadingController, private alertCtrl: AlertController) {
    this.screen.lock(this.screen.ORIENTATIONS.PORTRAIT).catch(function (error) { }); this.screen.onChange().subscribe(() => { });
    this.menu.enable(false);
  }
  type: string;

  ngOnInit() {
    this.type = 'reg';
  }

  inicio_login() {
    var register = JSON.parse(localStorage.getItem('registro'));
    if (!this.expresion.test(this.login_user.correo)) {
      this.em_err = true;
      setTimeout(() => {
        this.em_err = false;
      }, 1200);
    } else {
      if (this.login_user.correo === this.service.response_login.correo || this.login_user.correo === register.correo) {
        if (this.login_user.password === this.service.response_login.password || this.login_user.password === register.password) {
          this.loading();
          setTimeout(() => {
            this.reg();
            this.service.signUp();
            let valor = '1';
            localStorage.setItem('inicio_login', valor);
            this.menu.enable(true);
          }, 1800);
        } else {
          this.cred_err = true;
          setTimeout(() => {
            this.cred_err = false;
          }, 1200);
        }
      } else {
        this.usr_err = true;
        setTimeout(() => {
          this.usr_err = false;
        }, 1200);
      }
    }
  }

  inicio_registro() {
    var register = JSON.parse(localStorage.getItem('registro'));
    if (register === null) {
      if (!this.expresion.test(this.registro_user.correo)) {
        this.em_error = true;
        setTimeout(() => {
          this.em_error = false;
        }, 1200);
      } else {
        if (this.registro_user.password === this.confirmar) {
          this.loading();
          setTimeout(() => {
            let valor = '1';
            localStorage.setItem('inicio_registro', valor);
            this.service.login(this.registro_user);
            this.log();
            this.menu.enable(true);
          }, 1800);
        } else {
          this.pass_error = true;
          setTimeout(() => {
            this.pass_error = false;
          }, 1200);
        }
      }
    } else {
      if (this.registro_user.correo === register.correo) {
        this.user_exist = true;
        setTimeout(() => {
          this.user_exist = false;
        }, 1200);
      } else {
        this.r_error = true;
        setTimeout(() => {
          this.r_error = false;
        }, 1200);
      }
    }
  }

  windowPassword() {
    this.showPrompt();
  }
  async showPrompt() {
    const prompt = await this.alertCtrl.create({
      message: "Recuperar contraseña",
      inputs: [{
        name: 'email',
        placeholder: 'Correo electrónico',
        type: 'email'
      }, {
        name: 'password',
        placeholder: 'Contraseña',
        type: 'password'
      }, {
        name: 'confirm',
        placeholder: 'Confirmar contraseña',
        type: 'password'
      }],
      buttons: [{
        text: 'Cancelar',
        handler: data => {
        }
      },
      {
        text: 'Guardar',
        handler: data => {
          if (data.email === "" || data.password === "" || data.confirm === "") {
            let sms = 'Campos Incompletos';
            this.presentAlert(sms);
            return false;
          } else {
            if (!this.expresion.test(data.email)) {
              let sms = 'Correo invalido';
              this.presentAlert(sms);
              return false;
            } else {
              if (data.password != data.confirm) {
                let sms = 'Las contraseñas no coinciden';
                this.presentAlert(sms);
                return false;
              } else {
                this.loading();
              }
            }
          }
        }
      }
      ],
    });
    await prompt.present();
  }

  isLoading = false;
  async loading() {
    this.isLoading = true;
    return await this.loadingController.create({
      message: 'Cargando',
      duration: 2000
    }).then(a => {
      a.present().then(() => {
        console.log('loading presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort laoding'));
        }
      });
    });
  }

  async presentAlert(sms) {
    const alert = await this.alertCtrl.create({
      //header: 'Error',
      message: sms,
      buttons: ['OK']
    });
    await alert.present();
  }

  redes = false;
  inicio_() {
    this.loader = true;
    setTimeout(() => {
      this.loader = false
      this.texto = 'Cargando...';
      this.inicio = false;
      this.login_registro = true;
      this.redes = true;
    }, 1000);
  }
  reg() {
    this.login_user.correo = null;
    this.login_user.password = null;
    this.lost = false;
  }
  lost = false;
  log() {
    this.registro_user.nombre = null;
    this.registro_user.apellido = null;
    this.registro_user.correo = null;
    this.registro_user.password = null;
    this.confirmar = null;
    this.lost = true;
  }

}
