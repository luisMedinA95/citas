import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { ServicioService } from '../services/servicio.service';

@Component({
  selector: 'app-popover',
  templateUrl: './popover.page.html',
  styleUrls: ['./popover.page.scss'],
})
export class PopoverPage implements OnInit {
  opcion = [
    {
      id_opcion: '1',
      opcion_: 'Hombres',
      grupo: [
        {
          idUsuario: '1.1',
          edadUs: '18 - 25 años'
        },
        {
          idUsuario: '1.2',
          edadUs: '25 - 35 años'
        },
        {
          idUsuario: '1.3',
          edadUs: '35 - 45 años'
        },
        {
          idUsuario: '1.4',
          edadUs: '45 en adelante'
        }
      ]
    },
    {
      id_opcion: '2',
      opcion_: 'Mujeres',
      grupo: [
        {
          idUsuario: '2.1',
          edadUs: '18 - 25 años'
        },
        {
          idUsuario: '2.2',
          edadUs: '25 - 35 años'
        },
        {
          idUsuario: '2.3',
          edadUs: '35 - 45 años'
        },
        {
          idUsuario: '2.4',
          edadUs: '45 en adelante'
        }
      ]
    }
  ];
  estado;
  constructor(private popoverController: PopoverController, private service: ServicioService) {
    this.estado = this.service.estados;
  }
  ngOnInit() {
  }
  
  valores;
  onClick(val, sb, s) {
    this.valores = val;
    const bor = document.getElementById('sub' + s);
    bor.style.backgroundColor = '#000000 ';
    bor.style.color = '#ffffff';
    bor.setAttribute("selected", "");

    setTimeout(() => {
      this.popoverController.dismiss({
        opcionMenu: this.valores.id_opcion + " " + this.valores.opcion_,
        opcionSubmenu: sb
      })
    }, 300);

    for (let i = 0; i < this.valores.grupo.length; i++) {
      // console.log(this.valores.grupo[i]);
    }
  }
vari;
Seleccionar_Todo = false;

cucumber: boolean;
updateCucumber() {
  console.log('Nuevo estado:' + this.cucumber);
}

hombres;
mujeres;

obtener(v){
  
    if(v.target.id === 'nivel1-1'){
    this.hombres = this.opcion[0].grupo;

  
    }else{
      if(v.target.id === 'nivel1-2'){
        this.mujeres = this.opcion[1].grupo;
      }
    }

    /*
    setTimeout(() => {
      this.popoverController.dismiss({
      })
    }, 300);
     */
  }


}
