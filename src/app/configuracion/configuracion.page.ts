import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { NavController, MenuController, ModalController, AlertController } from '@ionic/angular';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { Injectable } from '@angular/core';
import { UpdateUserComponent } from '../components/update-user/update-user.component';
import { ServicioService } from '../services/servicio.service';

@Component({
  selector: 'app-configuracion',
  templateUrl: './configuracion.page.html',
  styleUrls: ['./configuracion.page.scss'],
})
@Injectable()
export class ConfiguracionPage implements OnInit {
  @Input() texto = 'Cargando...';
  loader = false;
  nombre;
  correo;
  pass;
  config_foto = 'https://fotos.subefotos.com/895908d9572e55e42c9128e3ccd4ade7o.jpg';
  expresion = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
  constructor(private navCtrl: NavController, private menu: MenuController, public modalController: ModalController, private alertCtrl: AlertController,
    private screen: ScreenOrientation, private updUsr: UpdateUserComponent, private service: ServicioService) {
    this.screen.lock(this.screen.ORIENTATIONS.PORTRAIT).catch(function (error) { }); this.screen.onChange().subscribe(() => { });
    this.nombre = this.service.response_login.nombre + this.service.response_login.apellido;
    this.correo = this.service.response_login.correo;
    this.pass = this.service.response_login.password;
    this.menu.enable(false);
  }

  ngOnInit() {
  }
  async presentAlertRadio() {
  const alert = await this.alertCtrl.create({
    cssClass: 'my-custom-class',
    header: 'Prompt!',
    inputs: [
      {
        name: 'name1',
        type: 'text',
        placeholder: 'Placeholder 1'
      },
      {
        name: 'name2',
        type: 'text',
        id: 'name2-id',
        value: 'hello',
        placeholder: 'Placeholder 2'
      },
      // multiline input.
      {
        name: 'paragraph',
        id: 'paragraph',
        type: 'textarea',
        placeholder: 'Placeholder 3'
      },
      {
        name: 'name3',
        value: 'http://ionicframework.com',
        type: 'url',
        placeholder: 'Favorite site ever'
      },
      // input date with min & max
      {
        name: 'name4',
        type: 'date',
        min: '2017-03-01',
        max: '2018-01-12'
      },
      // input date without min nor max
      {
        name: 'name5',
        type: 'date'
      },
      {
        name: 'name6',
        type: 'number',
        min: -5,
        max: 10
      },
      {
        name: 'name7',
        type: 'number'
      },
      {
        name: 'name8',
        type: 'password',
        placeholder: 'Advanced Attributes',
        cssClass: 'specialClass',
        attributes: {
          maxlength: 4,
          inputmode: 'decimal'
        }
      }
    ],
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        cssClass: 'secondary',
        handler: () => {
          console.log('Confirm Cancel');
        }
      }, {
        text: 'Ok',
        handler: () => {
          console.log('Confirm Ok');
        }
      }
    ]
  });

  await alert.present();
}

  async presentAlertRadio2() {
    const alert = await this.alertCtrl.create({
      cssClass: 'my-custom-class',
      header: 'Radio',
      inputs: [
        {
          name: 'radio1',
          type: 'radio',
          label: 'Radio 1',
          value: 'value1',
          checked: true
        },
        {
          name: 'radio2',
          type: 'radio',
          label: 'Radio 2',
          value: 'value2'
        },
        {
          name: 'radio3',
          type: 'radio',
          label: 'Radio 3',
          value: 'value3'
        },
        {
          name: 'radio4',
          type: 'radio',
          label: 'Radio 4',
          value: 'value4'
        },
        {
          name: 'radio5',
          type: 'radio',
          label: 'Radio 5',
          value: 'value5'
        },
        {
          name: 'radio6',
          type: 'radio',
          label: 'Radio 6 Radio 6 Radio 6 Radio 6 Radio 6 Radio 6 Radio 6 Radio 6 Radio 6 Radio 6 ',
          value: 'value6'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: () => {
            console.log('Confirm Ok');
          }
        }
      ]
    });

    await alert.present();
  }


  async presentAlertCheckbox() {
    const alert = await this.alertCtrl.create({
      cssClass: 'my-custom-class',
      header: 'Checkbox',
      inputs: [
        {
          name: 'checkbox1',
          type: 'checkbox',
          label: 'Checkbox 1',
          value: 'value1',
          checked: true
        },

        {
          name: 'checkbox2',
          type: 'checkbox',
          label: 'Checkbox 2',
          value: 'value2'
        },

        {
          name: 'checkbox3',
          type: 'checkbox',
          label: 'Checkbox 3',
          value: 'value3'
        },

        {
          name: 'checkbox4',
          type: 'checkbox',
          label: 'Checkbox 4',
          value: 'value4'
        },

        {
          name: 'checkbox5',
          type: 'checkbox',
          label: 'Checkbox 5',
          value: 'value5'
        },

        {
          name: 'checkbox6',
          type: 'checkbox',
          label: 'Checkbox 6 Checkbox 6 Checkbox 6 Checkbox 6 Checkbox 6 Checkbox 6 Checkbox 6 Checkbox 6 Checkbox 6 Checkbox 6',
          value: 'value6'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: () => {
            console.log('Confirm Ok');
          }
        }
      ]
    });

    await alert.present();
  }

  updateNickname() {
    this.showWindowNickname();
  }
  async showWindowNickname() {
    const prompt = await this.alertCtrl.create({
      message: "Actualizar Apodo",
      inputs: [{
        name: 'apodo',
        placeholder: 'Apodo',
        type: 'text'
      }],
      buttons: [{
        text: 'Cancelar',
        handler: data => { }
      }, {
        text: 'Guardar',
        handler: data => {
          if (data.apodo === "" || data.apodo === undefined || data.apodo === null) {
            let sms = 'Campos Incompletos';
            this.presentAlert(sms);
            return false;
          }else{
            //
          }
        }
      }],
    });
    await prompt.present();
  }
  updateEmail() {
    this.showWindowEmail();
  }
  async showWindowEmail() {
    const prompt = await this.alertCtrl.create({
      message: "Actualizar correo electrónico",
      inputs: [{
        name: 'mail',
        placeholder: 'Correo electrónico',
        type: 'email'
      }],
      buttons: [{
        text: 'Cancelar',
        handler: data => {  }
      },{
        text: 'Guardar',
        handler: data => {
          if (data.mail === "" || data.mail === undefined || data.mail === null) {
            let sms = 'Campos Incompletos';
            this.presentAlert(sms);
            return false;
          } else {
            if (!this.expresion.test(data.mail)) {
              let sms = 'Correo electrónico incorrecto';
              this.presentAlert(sms);
              return false;
            } else {
              //
            }
          }
        }
      }],
    });
    await prompt.present();
  }
  updatePassword() {
    this.showWindowPassword();
  }

  async showWindowPassword() {
    const prompt = await this.alertCtrl.create({
      message: "Actualizar Contraseña",
      inputs: [{
        name: 'pass',
        placeholder: 'Contraseña',
        type: 'password'
      },{
        name: 'confirm',
        placeholder: 'Confirmar contraseña',
        type: 'password'
      }],
      buttons: [{
        text: 'Cancelar',
        handler: data => {  }
      },{
        text: 'Guardar',
        handler: data => {
          if (data.pass === "" || data.pass === undefined || data.pass === null || data.confirm === "" || data.confirm === undefined || data.confirm === null) {
            let sms = 'Campos Incompletos';
            this.presentAlert(sms);
            return false;
          } else {
            if (data.pass != data.confirm) {
              let sms = 'Las contraseñas no coinciden';
              this.presentAlert(sms);
              return false;
            } else {
              //
            }
          }
        }
      }],
    });
    await prompt.present();
  }


  async presentAlert(sms) {
    const alert = await this.alertCtrl.create({
      message: sms,
      buttons: ['OK']
    });
    await alert.present();
  }

  back() {
    this.menu.enable(true);
    this.loader = true;
    setTimeout(() => {
      this.loader = false;
      this.texto = 'Cargando...';
      this.navCtrl.navigateForward('/folder/Inbox');
    }, 1000);
  }
}
