import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConfiguracionPageRoutingModule } from './configuracion-routing.module';

import { ConfiguracionPage } from './configuracion.page';
//import { ModalUserPage } from '../modals/modal-user/modal-user.page';   //./modal-user.page
//import { ModalUserPage } from '../app/modals/modal-user/modal-user.page';

import { UpdateUserComponent } from '../components/update-user/update-user.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConfiguracionPageRoutingModule
  ],
  declarations: [ConfiguracionPage],
  entryComponents: []   //ModalUserPage, ModalUserPage////UpdateUserComponent
})
export class ConfiguracionPageModule {}
