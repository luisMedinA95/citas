import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { NavController, Platform } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';
import { Observable, from } from 'rxjs';
import { ipglobal } from '../variablesGlobales';
@Injectable({
  providedIn: 'root'
})
export class ServicioService {
  dir = ipglobal.ip;
  userById = this.dir + 'userId/';
  users;
  perfiles_provider = [
    {
      perfilId: '1',
      apodo: 'Cariñoso',
      municipio: 'Ixtlahuaca',
      nombre: 'Luis',
      apellido: 'Medina',
      fotoMin: 'https://fotos.subefotos.com/95da47dc02e21fc067f545004b28c836o.jpg',
      fotoPerfil: 'https://fotos.subefotos.com/30a7e45d5a5d7e4a0221182c91622239o.jpg',
      fotos: [{
        imagen: 'https://fotos.subefotos.com/95da47dc02e21fc067f545004b28c836o.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/30a7e45d5a5d7e4a0221182c91622239o.jpg'
      }
      ],
      descripcion: 'Me considero un chico buena onda, no suelo salir tanto, me gusta el anime, futbol y en mis ratos libres me gusta programar',
      meInteresa: '5',
      activo: true,
      status: 'ahora',
      edad: '24 años',
      lat: '19.5715244',
      long: '-99.7715244',
      correo: 'luism3039@gmail.com',
      cumpleanios: '28/12/1995',
      redes:[
        {
          facebook: true,
          user: 'luis/medinamauro'
        },
        {
          tumblr: false,
          user: 'luismedina95'
        },
        {
          whatsapp: true,
          number: '5645328745'
        },
        {
          twitter: false,
          user: 'luis_medina'
        },
        {
          instagram: true,
          user: 'luis_medina'
        }
      ],
      acercaDeMi: [
        {
          aptitudes: []
        },
        {
          habilidades: [
            {
              trabajoEquipo:  true
            },
            {
              comunicacion:  true
            },
            {
              organizacion:  true
            },
            {
              inovarCrear:  true
            }
          ]
        },
        {
          pasatiempos: [
            {
              futbol:  true
            },
            {
              ejercicio:  true
            },
            {
              natacion:  true
            },
            {
              ciclismo:  true
            },
            {
              atletismo:  true
            },
            {
              musica:  true
            },
            {
              leer:  true
            },
            {
              comer:  true
            }
          ]
        }
      ]
    },
    {
      perfilId: '2',
      apodo: 'Mamasita',
      municipio: 'Ixtlahuaca',
      nombre: 'Ines',
      apellido: 'Torres',
      fotoMin: 'https://fotos.subefotos.com/0b1c4eb501e22315144e2b727d3b0a53o.jpg',
      fotoPerfil: 'https://fotos.subefotos.com/0b1c4eb501e22315144e2b727d3b0a53o.jpg',
      fotos: [{
        imagen: 'https://fotos.subefotos.com/91c1bb79d0365a014ce9ab80e637d283o.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/38ad9eeeae597d1edd65999cf9d05399o.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/b28335efc56f7d2bb2811f6c8f7a4873o.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/204c5fa6b4ad5cea1354cd72ad97a455o.jpg'
      },
      ],
      descripcion: 'Soy una chica muy candente, me gusta tener iniciativa, me gusta lo pervertido y tambien se querer muy bonito',
      meInteresa: '5',
      activo: false,
      status: 'hace 1hr',
      edad: '30 años',
      lat: '19.5715244',
      long: '-99.7715244',
      correo: 'user39@gmail.com',
      cumpleanios: '28/12/1999',
      redes:[
        {
          facebook: true,
          user: 'user/desc'
        },
        {
          tumblr: true,
          user: 'descUser'
        },
        {
          whatsapp: true,
          number: '9345328745'
        },
        {
          twitter: true,
          user: 'user_desc'
        },
        {
          instagram: false,
          user: 'desc_us'
        }
      ],
      acercaDeMi: [
        {
          aptitudes: [
            {
              creatividad:  true
            },
            {
              liderazgo:  false
            },
            {
              seriedad:  true
            }
          ]
        },
        {
          habilidades: []
        },
        {
          pasatiempos: [
            {
              futbol:  false
            },
            {
              ejercicio:  true
            },
            {
              natacion:  true
            },
            {
              ciclismo:  false
            },
            {
              atletismo:  true
            },
            {
              musica:  false
            },
            {
              leer:  true
            },
            {
              comer:  false
            }
          ]
        }
      ]
    },
    {
      perfilId: '3',
      apodo: 'Bonita MRG',
      municipio: 'San Felipe',
      nombre: 'Marycarmen',
      apellido: 'Ruiz',
      fotoMin: 'https://fotos.subefotos.com/f6186afdc344312842a40ec7d05ff233o.jpg',
      fotoPerfil: 'https://fotos.subefotos.com/f6186afdc344312842a40ec7d05ff233o.jpg',
      fotos: [{
        imagen: 'https://fotos.subefotos.com/6841152a60b8b6b3a9bb7f7dd0b27ddco.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/a856707d0d307b6a1da524e5482d5c36o.jpg'
      }
      ],
      descripcion: 'Soy una chica atenta, detallista, me gusta salir a convivir con amigos',
      meInteresa: '35',
      activo: false,
      status: 'hace 2min',
      edad: '22 años',
      lat: '19.7142243',
      long: '-99.9592461',
      correo: 'user39@gmail.com',
      cumpleanios: '28/12/1999',
      redes:[
        {
          facebook: false,
          user: 'user/desc'
        },
        {
          tumblr: true,
          user: 'descUser'
        },
        {
          whatsapp: true,
          number: '9345328745'
        },
        {
          twitter: true,
          user: 'user_desc'
        },
        {
          instagram: false,
          user: 'desc_us'
        }
      ],
      acercaDeMi: [
        {
          aptitudes: [
            {
              creatividad:  true
            },
            {
              liderazgo:  false
            },
            {
              seriedad:  true
            }
          ]
        },
        {
          habilidades: [
            {
              trabajoEquipo:  false
            },
            {
              comunicacion:  true
            },
            {
              organizacion:  true
            },
            {
              inovarCrear:  true
            }
          ]
        },
        {
          pasatiempos: []
        }
      ]
    },
    {
      perfilId: '4',
      apodo: 'Sexy AS',
      municipio: 'San Felipe',
      nombre: 'Azucena',
      apellido: 'Segundo',
      fotoMin: 'https://fotos.subefotos.com/26019ccec32c7b72d8f213d64eb0a238o.jpg',
      fotoPerfil: 'https://fotos.subefotos.com/1de6054ef19526080799a45afeda90dao.jpg',
      fotos: [{
        imagen: 'https://fotos.subefotos.com/8e79134b10189ec35cb78d63a53e3146o.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/d5a85598a406a0b565267cf74a2cb51bo.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/86d4ebd2ab82ddd005ba0033ecaf5b14o.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/872c3cbe01052f897f9d70dbb7e08ba8o.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/54a38208471dd8a87e3879c4a60607d0o.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/8c96c5de90b0acb92f171bacf2ee9f54o.jpg'
      }
      ],
      descripcion: 'Soy una chica muy buena onda, detallista, me gusta salir a convivir con amigos',
      meInteresa: '52',
      activo: true,
      status: 'ahora',
      edad: '24 años',
      lat: '19.7142243',
      long: '-99.9592461',
      correo: 'user39@gmail.com',
      cumpleanios: '28/12/1999',
      redes:[
        {
          facebook: false,
          user: 'user/desc'
        },
        {
          tumblr: true,
          user: 'descUser'
        },
        {
          whatsapp: true,
          number: '9345328745'
        },
        {
          twitter: true,
          user: 'user_desc'
        },
        {
          instagram: false,
          user: 'desc_us'
        }
      ],
      acercaDeMi: [
        {
          aptitudes: [
            {
              creatividad:  true
            },
            {
              liderazgo:  false
            },
            {
              seriedad:  true
            }
          ]
        },
        {
          habilidades: [
            {
              trabajoEquipo:  false
            },
            {
              comunicacion:  false
            },
            {
              organizacion:  true
            },
            {
              inovarCrear:  false
            }
          ]
        },
        {
          pasatiempos: [
            {
              futbol:  false
            },
            {
              ejercicio:  true
            },
            {
              natacion:  true
            },
            {
              ciclismo:  false
            },
            {
              atletismo:  true
            },
            {
              musica:  false
            },
            {
              leer:  true
            },
            {
              comer:  false
            }
          ]
        }
      ]
    },
    {
      perfilId: '5',
      apodo: 'Preciosa AG',
      municipio: 'Ixtlahuaca',
      nombre: 'Angelica',
      apellido: 'Cruz',
      fotoMin: 'https://fotos.subefotos.com/e0e33ab4b0ed1fd07216619a2b9483afo.jpg',
      fotoPerfil: 'https://fotos.subefotos.com/3fbbe1e114fb6a941ea103e59c8b8382o.jpg',
      fotos: [{
        imagen: 'https://fotos.subefotos.com/302f59cff04b3b42a94dc37f8ce1a1beo.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/6446356f2324be463170f370d15999bco.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/0f0ba72c52d7e1be014133d0a3461dd7o.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/b227497c5c7a3302ec09ae3935cce4d6o.jpg'
      }
      ],
      descripcion: 'Soy una chica muy buena onda, fiel, detallista, me gusta salir a convivir con amigos',
      meInteresa: '59',
      activo: true,
      status: 'ahora',
      edad: '24 años',
      lat: '19.5715244',
      long: '-99.7715244',
      correo: 'user39@gmail.com',
      cumpleanios: '28/12/1999',
      redes:[
        {
          facebook: false,
          user: 'user/desc'
        },
        {
          tumblr: true,
          user: 'descUser'
        },
        {
          whatsapp: false,
          number: '9345328745'
        },
        {
          twitter: true,
          user: 'user_desc'
        },
        {
          instagram: false,
          user: 'desc_us'
        }
      ],
      acercaDeMi: [
        {
          aptitudes: [
            {
              creatividad:  true
            },
            {
              liderazgo:  false
            },
            {
              seriedad:  true
            }
          ]
        },
        {
          habilidades: [
            {
              trabajoEquipo:  true
            },
            {
              comunicacion:  true
            },
            {
              organizacion:  true
            },
            {
              inovarCrear:  true
            }
          ]
        },
        {
          pasatiempos: [
            {
              futbol:  false
            },
            {
              ejercicio:  false
            },
            {
              natacion:  false
            },
            {
              ciclismo:  false
            },
            {
              atletismo:  false
            },
            {
              musica:  false
            },
            {
              leer:  true
            },
            {
              comer:  false
            }
          ]
        }
      ]
    },
    {
      perfilId: '6',
      apodo: 'Divina EV',
      municipio: 'San Felipe',
      nombre: 'Erika',
      apellido: 'Valdez',
      fotoMin: 'https://fotos.subefotos.com/754ab13093608b0a3db035f1cd315189o.jpg',
      fotoPerfil: 'https://fotos.subefotos.com/90968e7567bf2e77791681aa9fe40283o.jpg',
      fotos: [{
        imagen: 'https://fotos.subefotos.com/8f5aa0d9169311a347342461495e6c88o.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/1edd1bb9093a33c4bcc3bcf4a937b87co.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/2eea7b8ca74f2b93812d467ef0994847o.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/98cc4637014fa1e502a421a8fbcb45dbo.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/dd4659aa1e285c064e68f93682c3903ao.jpg'
      }
      ],
      descripcion: 'Soy una chica muy buena onda, detallista, me gusta salir a convivir con amigos',
      meInteresa: '95',
      activo: true,
      status: 'ahora',
      edad: '24 años',
      lat: '19.7142243',
      long: '-99.9592461',
      correo: 'user39@gmail.com',
      cumpleanios: '28/12/1999',
      redes:[
        {
          facebook: false,
          user: 'user/desc'
        },
        {
          tumblr: true,
          user: 'descUser'
        },
        {
          whatsapp: true,
          number: '9345328745'
        },
        {
          twitter: true,
          user: 'user_desc'
        },
        {
          instagram: false,
          user: 'desc_us'
        }
      ],
      acercaDeMi: [
        {
          aptitudes: [
            {
              creatividad:  true
            },
            {
              liderazgo:  false
            },
            {
              seriedad:  true
            }
          ]
        },
        {
          habilidades: [
            {
              trabajoEquipo:  false
            },
            {
              comunicacion:  false
            },
            {
              organizacion:  true
            },
            {
              inovarCrear:  false
            }
          ]
        },
        {
          pasatiempos: [
            {
              futbol:  false
            },
            {
              ejercicio:  true
            },
            {
              natacion:  true
            },
            {
              ciclismo:  false
            },
            {
              atletismo:  true
            },
            {
              musica:  false
            },
            {
              leer:  true
            },
            {
              comer:  false
            }
          ]
        }
      ]
    },
    {
      perfilId: '7',
      apodo: 'Linda BG',
      municipio: 'Ixtlahuaca',
      nombre: 'Brissa',
      apellido: 'Garnica',
      fotoMin: 'https://fotos.subefotos.com/3dfb82a039df25fbe208913948b980dao.jpg',
      fotoPerfil: 'https://fotos.subefotos.com/cd1d6deff583b4ed6ae895e16a97d1b2o.jpg',
      fotos: [{
        imagen: 'https://fotos.subefotos.com/94b68f3bae2c497c506000b036b51757o.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/3c3280b1a0100275681e80507e22656do.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/cd1d6deff583b4ed6ae895e16a97d1b2o.jpg'
      }
      ],
      descripcion: 'Soy una chica muy buena onda, fiel detallista, me gusta salir a convivir con amigos',
      meInteresa: '65',
      activo: false,
      status: 'hace 7min',
      edad: '24 años',
      lat: '19.5715244',
      long: '-99.7715244',
      correo: 'user39@gmail.com',
      cumpleanios: '28/12/1999',
      redes:[
        {
          facebook: false,
          user: 'user/desc'
        },
        {
          tumblr: true,
          user: 'descUser'
        },
        {
          whatsapp: true,
          number: '9345328745'
        },
        {
          twitter: true,
          user: 'user_desc'
        },
        {
          instagram: false,
          user: 'desc_us'
        }
      ],
      acercaDeMi: [
        {
          aptitudes: [
            {
              creatividad:  true
            },
            {
              liderazgo:  false
            },
            {
              seriedad:  true
            }
          ]
        },
        {
          habilidades: [
            {
              trabajoEquipo:  false
            },
            {
              comunicacion:  false
            },
            {
              organizacion:  true
            },
            {
              inovarCrear:  false
            }
          ]
        },
        {
          pasatiempos: [
            {
              futbol:  false
            },
            {
              ejercicio:  true
            },
            {
              natacion:  true
            },
            {
              ciclismo:  false
            },
            {
              atletismo:  true
            },
            {
              musica:  false
            },
            {
              leer:  true
            },
            {
              comer:  false
            }
          ]
        }
      ]
    },
    {
      perfilId: '8',
      apodo: 'Guapa AS',
      municipio: 'Ixtlahuaca',
      nombre: 'Angelica',
      apellido: 'Sanchez',
      fotoMin: 'https://fotos.subefotos.com/08a2611994857e1c54ea6e688d54531eo.jpg',
      fotoPerfil: 'https://fotos.subefotos.com/be289ff2b0fcdec5f7293b7d85cb1ad1o.jpg',
      fotos: [{
        imagen: 'https://fotos.subefotos.com/08a2611994857e1c54ea6e688d54531eo.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/3f9d78a2bdcb72dc73e8af179baafe93o.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/af821a29f24f4f46df495e541e76b190o.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/e4042a9ce76cb6ecf75d3ca302d3b8b9o.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/e0bf6a0a1bd2330e670ba106e91640f9o.jpg'
      }
      ],
      descripcion: 'Soy una chica muy buena onda, fiel, detallista, me gusta salir a convivir con amigos',
      meInteresa: '22',
      activo: true,
      status: 'ahora',
      edad: '24 años',
      lat: '19.5715244',
      long: '-99.7715244',
      correo: 'user39@gmail.com',
      cumpleanios: '28/12/1999',
      redes:[
        {
          facebook: false,
          user: 'user/desc'
        },
        {
          tumblr: true,
          user: 'descUser'
        },
        {
          whatsapp: true,
          number: '9345328745'
        },
        {
          twitter: true,
          user: 'user_desc'
        },
        {
          instagram: false,
          user: 'desc_us'
        }
      ],
      acercaDeMi: [
        {
          aptitudes: [
            {
              creatividad:  true
            },
            {
              liderazgo:  false
            },
            {
              seriedad:  true
            }
          ]
        },
        {
          habilidades: [
            {
              trabajoEquipo:  false
            },
            {
              comunicacion:  false
            },
            {
              organizacion:  false
            },
            {
              inovarCrear:  false
            }
          ]
        },
        {
          pasatiempos: [
            {
              futbol:  false
            },
            {
              ejercicio:  true
            },
            {
              natacion:  true
            },
            {
              ciclismo:  true
            },
            {
              atletismo:  true
            },
            {
              musica:  true
            },
            {
              leer:  true
            },
            {
              comer:  true
            }
          ]
        }
      ]
    },
    {
      perfilId: '9',
      apodo: 'Guapa AS',
      municipio: 'Ixtlahuaca',
      nombre: 'Joss',
      apellido: 'Martinez',
      fotoMin: 'https://fotos.subefotos.com/58045ea776ceedfe044c9b784a878cc0o.jpg',
      fotoPerfil: 'https://fotos.subefotos.com/be289ff2b0fcdec5f7293b7d85cb1ad1o.jpg',
      fotos: [{
        imagen: 'https://fotos.subefotos.com/be289ff2b0fcdec5f7293b7d85cb1ad1o.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/086a21361418f2e658f1abfd191079bco.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/f94ce70f1cbd54a41a60b3b148dfd25bo.jpg'
      }
      ],
      descripcion: 'Soy una chica muy buena onda, fiel, detallista, me gusta salir a convivir con amigos',
      meInteresa: '22',
      activo: false,
      status: 'hace 9hr',
      edad: '24 años',
      lat: '19.5715244',
      long: '-99.7715244',
      correo: 'user39@gmail.com',
      cumpleanios: '28/12/1999',
      redes:[
        {
          facebook: false,
          user: 'user/desc'
        },
        {
          tumblr: true,
          user: 'descUser'
        },
        {
          whatsapp: true,
          number: '9345328745'
        },
        {
          twitter: true,
          user: 'user_desc'
        },
        {
          instagram: false,
          user: 'desc_us'
        }
      ],
      acercaDeMi: [
        {
          aptitudes: [
            {
              creatividad:  true
            },
            {
              liderazgo:  false
            },
            {
              seriedad:  true
            }
          ]
        },
        {
          habilidades: [
            {
              trabajoEquipo:  false
            },
            {
              comunicacion:  false
            },
            {
              organizacion:  true
            },
            {
              inovarCrear:  false
            }
          ]
        },
        {
          pasatiempos: [
            {
              futbol:  true
            },
            {
              ejercicio:  true
            },
            {
              natacion:  true
            },
            {
              ciclismo:  true
            },
            {
              atletismo:  true
            },
            {
              musica:  true
            },
            {
              leer:  true
            },
            {
              comer:  true
            }
          ]
        }
      ]
    },
    {
      perfilId: '10',
      apodo: 'Guapa AS',
      municipio: 'San Felipe',
      nombre: 'Adelina',
      apellido: 'Martinez',
      fotoMin: 'https://fotos.subefotos.com/5654211f673a19aeac4d04825249c07bo.jpg',
      fotoPerfil: 'https://fotos.subefotos.com/be289ff2b0fcdec5f7293b7d85cb1ad1o.jpg',
      fotos: [
        {
          imagen: 'https://fotos.subefotos.com/be289ff2b0fcdec5f7293b7d85cb1ad1o.jpg'
        },
        {
          imagen: 'https://fotos.subefotos.com/adbe329f7f046dff3b5ecf334a23cebfo.jpg'
        },
        {
          imagen: 'https://fotos.subefotos.com/78f4c013453b48e14d37f033d93ea335o.jpg'
        },
        {
          imagen: 'https://fotos.subefotos.com/470041198dd6c7d052a072e1e4e49ef3o.jpg'
        },
        {
          imagen: 'https://fotos.subefotos.com/10c7fd9531195a5e82e7c72cc7a35f17o.jpg'
        }
      ],
      descripcion: 'Soy una chica muy buena onda, fiel, detallista, me gusta salir a convivir con amigos',
      meInteresa: '22',
      activo: true,
      status: 'ahora',
      edad: '24 años',
      lat: '19.7142243',
      long: '-99.9592461',
      correo: 'user39@gmail.com',
      cumpleanios: '28/12/1999',
      redes:[
        {
          facebook: false,
          user: 'user/desc'
        },
        {
          tumblr: true,
          user: 'descUser'
        },
        {
          whatsapp: true,
          number: '9345328745'
        },
        {
          twitter: true,
          user: 'user_desc'
        },
        {
          instagram: false,
          user: 'desc_us'
        }
      ],
      acercaDeMi: [
        {
          aptitudes: [
            {
              creatividad:  true
            },
            {
              liderazgo:  false
            },
            {
              seriedad:  true
            }
          ]
        },
        {
          habilidades: [
            {
              trabajoEquipo:  false
            },
            {
              comunicacion:  false
            },
            {
              organizacion:  true
            },
            {
              inovarCrear:  false
            }
          ]
        },
        {
          pasatiempos: [
            {
              futbol:  false
            },
            {
              ejercicio:  true
            },
            {
              natacion:  true
            },
            {
              ciclismo:  false
            },
            {
              atletismo:  true
            },
            {
              musica:  false
            },
            {
              leer:  true
            },
            {
              comer:  false
            }
          ]
        }
      ]
    },
    {
      perfilId: '11',
      apodo: 'Guapa AS',
      municipio: 'Ixtlahuaca',
      nombre: 'Berenice',
      apellido: 'Martinez',
      fotoMin: 'https://fotos.subefotos.com/2a1b2f128a52c7fa64d9bb75f83d7b60o.jpg',
      fotoPerfil: 'https://fotos.subefotos.com/be289ff2b0fcdec5f7293b7d85cb1ad1o.jpg',
      fotos: [
        {
          imagen: 'https://fotos.subefotos.com/f866518a1c860056edc02acbbb954f8do.jpg'
        },
        {
          imagen: 'https://fotos.subefotos.com/1fbd7f271ca6d3809914bcf47ea625b9o.jpg'
        },
        {
          imagen: 'https://fotos.subefotos.com/2a1b2f128a52c7fa64d9bb75f83d7b60o.jpg'
        },
        {
          imagen: 'https://fotos.subefotos.com/5992f5f83984b4e7a7b30f7a4f022f21o.jpg'
        },
        {
          imagen: 'https://fotos.subefotos.com/2a3c71640d66ddde95742f1eaea75172o.jpg'
        },
        {
          imagen: 'https://fotos.subefotos.com/ccee76f2f5acccc999d7435ca3230392o.jpg'
        }
      ],
      descripcion: 'Soy una chica muy buena onda, fiel, detallista, me gusta salir a convivir con amigos',
      meInteresa: '22',
      activo: true,
      status: 'ahora',
      edad: '24 años',
      lat: '19.5715244',
      long: '-99.7715244',
      correo: 'user39@gmail.com',
      cumpleanios: '28/12/1999',
      redes:[
        {
          facebook: false,
          user: 'user/desc'
        },
        {
          tumblr: true,
          user: 'descUser'
        },
        {
          whatsapp: true,
          number: '9345328745'
        },
        {
          twitter: true,
          user: 'user_desc'
        },
        {
          instagram: false,
          user: 'desc_us'
        }
      ],
      acercaDeMi: [
        {
          aptitudes: [
            {
              creatividad:  true
            },
            {
              liderazgo:  false
            },
            {
              seriedad:  true
            }
          ]
        },
        {
          habilidades: [
            {
              trabajoEquipo:  false
            },
            {
              comunicacion:  false
            },
            {
              organizacion:  true
            },
            {
              inovarCrear:  false
            }
          ]
        },
        {
          pasatiempos: [
            {
              futbol:  false
            },
            {
              ejercicio:  true
            },
            {
              natacion:  true
            },
            {
              ciclismo:  false
            },
            {
              atletismo:  true
            },
            {
              musica:  false
            },
            {
              leer:  true
            },
            {
              comer:  false
            }
          ]
        }
      ]
    },
    {
      perfilId: '12',
      apodo: 'Guapa AS',
      municipio: 'Jocotitlan',
      nombre: 'Britty',
      apellido: 'Ramos',
      fotoMin: 'https://fotos.subefotos.com/26c7e8b5414a13179ce80e365d8c441do.jpg',
      fotoPerfil: 'https://fotos.subefotos.com/be289ff2b0fcdec5f7293b7d85cb1ad1o.jpg',
      fotos: [{
        imagen: 'https://fotos.subefotos.com/26190cc0d2cca64f850f3aa9ca560d4fo.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/26c7e8b5414a13179ce80e365d8c441do.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/98a4e5d597e3fcb7b4e4c79236f559e9o.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/1c7ca2c3ee70423171d31de06302176ao.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/5c51ac7b1be8f855a597ad0a55f9467eo.jpg'
      }
      ],
      descripcion: 'Soy una chica muy buena onda, fiel, detallista, me gusta salir a convivir con amigos',
      meInteresa: '22',
      activo: false,
      status: 'hace 2hr',
      edad: '24 años',
      lat: '19.709188',
      long: '-99.8060332',
      correo: 'user39@gmail.com',
      cumpleanios: '28/12/1999',
      redes:[
        {
          facebook: false,
          user: 'user/desc'
        },
        {
          tumblr: true,
          user: 'descUser'
        },
        {
          whatsapp: true,
          number: '9345328745'
        },
        {
          twitter: true,
          user: 'user_desc'
        },
        {
          instagram: false,
          user: 'desc_us'
        }
      ],
      acercaDeMi: [
        {
          aptitudes: [
            {
              creatividad:  true
            },
            {
              liderazgo:  false
            },
            {
              seriedad:  true
            }
          ]
        },
        {
          habilidades: [
            {
              trabajoEquipo:  false
            },
            {
              comunicacion:  false
            },
            {
              organizacion:  true
            },
            {
              inovarCrear:  false
            }
          ]
        },
        {
          pasatiempos: [
            {
              futbol:  false
            },
            {
              ejercicio:  true
            },
            {
              natacion:  true
            },
            {
              ciclismo:  false
            },
            {
              atletismo:  true
            },
            {
              musica:  false
            },
            {
              leer:  true
            },
            {
              comer:  false
            }
          ]
        }
      ]
    },
    {
      perfilId: '13',
      apodo: 'Guapa AS',
      municipio: 'San Felipe',
      nombre: 'Leticia',
      apellido: 'Estevez',
      fotoMin: 'https://fotos.subefotos.com/b9a6af25b1ed190b46e6a88be0b0a638o.jpg',
      fotoPerfil: 'https://fotos.subefotos.com/be289ff2b0fcdec5f7293b7d85cb1ad1o.jpg',
      fotos: [{
        imagen: 'https://fotos.subefotos.com/50f5fb80018406b9be66441535816addo.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/b9a6af25b1ed190b46e6a88be0b0a638o.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/0d43aeae4acc0972f29f2c5b33e8b7b9o.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/62f8b1a7805b8a5b3a913b26e2f02979o.jpg'
      }
      ],
      descripcion: 'Soy una chica muy buena onda, fiel, detallista, me gusta salir a convivir con amigos',
      meInteresa: '22',
      activo: false,
      status: 'hace 3hr',
      edad: '24 años',
      lat: '19.7142243',
      long: '-99.9592461',
      correo: 'user39@gmail.com',
      cumpleanios: '28/12/1999',
      redes:[
        {
          facebook: false,
          user: 'user/desc'
        },
        {
          tumblr: true,
          user: 'descUser'
        },
        {
          whatsapp: true,
          number: '9345328745'
        },
        {
          twitter: true,
          user: 'user_desc'
        },
        {
          instagram: false,
          user: 'desc_us'
        }
      ],
      acercaDeMi: [
        {
          aptitudes: [
            {
              creatividad:  true
            },
            {
              liderazgo:  false
            },
            {
              seriedad:  true
            }
          ]
        },
        {
          habilidades: [
            {
              trabajoEquipo:  false
            },
            {
              comunicacion:  false
            },
            {
              organizacion:  true
            },
            {
              inovarCrear:  false
            }
          ]
        },
        {
          pasatiempos: [
            {
              futbol:  false
            },
            {
              ejercicio:  true
            },
            {
              natacion:  true
            },
            {
              ciclismo:  false
            },
            {
              atletismo:  true
            },
            {
              musica:  false
            },
            {
              leer:  true
            },
            {
              comer:  false
            }
          ]
        }
      ]
    },
    {
      perfilId: '14',
      apodo: 'Guapa AS',
      municipio: 'Ixtlahuaca',
      nombre: 'Carina',
      apellido: 'Reyes',
      fotoMin: 'https://fotos.subefotos.com/895908d9572e55e42c9128e3ccd4ade7o.jpg',
      fotoPerfil: 'https://fotos.subefotos.com/be289ff2b0fcdec5f7293b7d85cb1ad1o.jpg',
      fotos: [{
        imagen: 'https://fotos.subefotos.com/4740029b245bcc1dee428d4f7b2836e1o.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/895908d9572e55e42c9128e3ccd4ade7o.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/7287ab4c66b6d0b0302b7e0f2472c8cbo.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/cff5023fb8fd8a82b3fffebaa971f2a5o.jpg'
      }
      ],
      descripcion: 'Soy una chica muy buena onda, fiel, detallista, me gusta salir a convivir con amigos',
      meInteresa: '22',
      activo: false,
      status: 'hace 21min',
      edad: '24 años',
      lat: '19.5715244',
      long: '-99.7715244',
      correo: 'user39@gmail.com',
      cumpleanios: '28/12/1999',
      redes:[
        {
          facebook: false,
          user: 'user/desc'
        },
        {
          tumblr: true,
          user: 'descUser'
        },
        {
          whatsapp: true,
          number: '9345328745'
        },
        {
          twitter: true,
          user: 'user_desc'
        },
        {
          instagram: false,
          user: 'desc_us'
        }
      ],
      acercaDeMi: [
        {
          aptitudes: [
            {
              creatividad:  true
            },
            {
              liderazgo:  false
            },
            {
              seriedad:  true
            }
          ]
        },
        {
          habilidades: [
            {
              trabajoEquipo:  false
            },
            {
              comunicacion:  false
            },
            {
              organizacion:  true
            },
            {
              inovarCrear:  false
            }
          ]
        },
        {
          pasatiempos: [
            {
              futbol:  false
            },
            {
              ejercicio:  true
            },
            {
              natacion:  true
            },
            {
              ciclismo:  false
            },
            {
              atletismo:  true
            },
            {
              musica:  false
            },
            {
              leer:  true
            },
            {
              comer:  false
            }
          ]
        }
      ]
    },
    {
      perfilId: '15',
      apodo: 'Sexy AR',
      municipio: 'San Felipe',
      nombre: 'Angeles',
      apellido: 'Ruiz',
      fotoMin: 'https://fotos.subefotos.com/f61b7e292d96814dac8f8e5e63303628o.jpg',
      fotoPerfil: 'https://fotos.subefotos.com/f61b7e292d96814dac8f8e5e63303628o.jpg',
      fotos: [{
        imagen: 'https://fotos.subefotos.com/29fd1577799f52aafd176e886001c98ao.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/785f782bf24b634c5fa9b8e899dc4639o.jpg'
      }
      ],
      descripcion: 'Soy una chica muy buena onda, fiel, detallista, me gusta salir a convivir con amigos',
      meInteresa: '12',
      activo: false,
      status: 'hace 11min',
      edad: '26 años',
      lat: '19.7142243',
      long: '-99.9592461',
      correo: 'user39@gmail.com',
      cumpleanios: '28/12/1999',
      redes:[
        {
          facebook: false,
          user: 'user/desc'
        },
        {
          tumblr: true,
          user: 'descUser'
        },
        {
          whatsapp: true,
          number: '9345328745'
        },
        {
          twitter: true,
          user: 'user_desc'
        },
        {
          instagram: false,
          user: 'desc_us'
        }
      ],
      acercaDeMi: [
        {
          aptitudes: [
            {
              creatividad:  true
            },
            {
              liderazgo:  false
            },
            {
              seriedad:  true
            }
          ]
        },
        {
          habilidades: [
            {
              trabajoEquipo:  false
            },
            {
              comunicacion:  false
            },
            {
              organizacion:  true
            },
            {
              inovarCrear:  false
            }
          ]
        },
        {
          pasatiempos: [
            {
              futbol:  false
            },
            {
              ejercicio:  true
            },
            {
              natacion:  true
            },
            {
              ciclismo:  false
            },
            {
              atletismo:  true
            },
            {
              musica:  false
            },
            {
              leer:  true
            },
            {
              comer:  false
            }
          ]
        }
      ]
    },
    {
      perfilId: '16',
      apodo: 'Flaquita DG',
      municipio: 'San Felipe',
      nombre: 'Diana',
      apellido: 'Garcia',
      fotoMin: 'https://fotos.subefotos.com/23580a168907034d4f7234c4ba24a424o.jpg',
      fotoPerfil: 'https://fotos.subefotos.com/852947a171e5170866e2b768f7aff5fdo.jpg',
      fotos: [{
        imagen: 'https://fotos.subefotos.com/c86d77d1abdd1f71ba7541fde03ea895o.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/e6db31fbd015a95b3ba872e4856bfab0o.jpg'
      }, {
        imagen: 'https://fotos.subefotos.com/c13fe5cfa57058f44d1aae491435f614o.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/0075317b0757db8e56d05e08f8e710b7o.jpg'
      }
      ],
      descripcion: 'Soy una chica muy buena onda, fiel, detallista, me gusta salir a convivir con amigos',
      meInteresa: '32',
      activo: true,
      status: 'ahora',
      edad: '23 años',
      lat: '19.7142243',
      long: '-99.9592461',
      correo: 'user39@gmail.com',
      cumpleanios: '28/12/1999',
      redes:[
        {
          facebook: false,
          user: 'user/desc'
        },
        {
          tumblr: true,
          user: 'descUser'
        },
        {
          whatsapp: true,
          number: '9345328745'
        },
        {
          twitter: true,
          user: 'user_desc'
        },
        {
          instagram: false,
          user: 'desc_us'
        }
      ],
      acercaDeMi: [
        {
          aptitudes: [
            {
              creatividad:  true
            },
            {
              liderazgo:  false
            },
            {
              seriedad:  true
            }
          ]
        },
        {
          habilidades: [
            {
              trabajoEquipo:  false
            },
            {
              comunicacion:  false
            },
            {
              organizacion:  true
            },
            {
              inovarCrear:  false
            }
          ]
        },
        {
          pasatiempos: [
            {
              futbol:  false
            },
            {
              ejercicio:  true
            },
            {
              natacion:  true
            },
            {
              ciclismo:  false
            },
            {
              atletismo:  true
            },
            {
              musica:  false
            },
            {
              leer:  true
            },
            {
              comer:  false
            }
          ]
        }
      ]
    },
    {
      perfilId: '17',
      apodo: 'Luna JV',
      municipio: 'San Felipe',
      nombre: 'Janeth',
      apellido: 'Vargas',
      fotoMin: 'https://fotos.subefotos.com/579028000c87956520e2382e413d5613o.jpg',
      fotoPerfil: 'https://fotos.subefotos.com/275ff6c40dbf94e3598c77e3c55c1353o.jpg',
      fotos: [{
        imagen: 'https://fotos.subefotos.com/d27a5bd2ddef64d6a913f632dfd23e9co.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/275ff6c40dbf94e3598c77e3c55c1353o.jpg'
      }, {
        imagen: 'https://fotos.subefotos.com/ab34792ba5c717a76bdc2ee66b95baabo.jpg'
      }
      ],
      descripcion: 'Soy una chica muy buena onda, fiel, detallista, me gusta salir a convivir con amigos',
      meInteresa: '21',
      activo: true,
      status: 'ahora',
      edad: '25 años',
      lat: '19.7142243',
      long: '-99.9592461',
      correo: 'user39@gmail.com',
      cumpleanios: '28/12/1999',
      redes:[
        {
          facebook: false,
          user: 'user/desc'
        },
        {
          tumblr: true,
          user: 'descUser'
        },
        {
          whatsapp: true,
          number: '9345328745'
        },
        {
          twitter: true,
          user: 'user_desc'
        },
        {
          instagram: false,
          user: 'desc_us'
        }
      ],
      acercaDeMi: [
        {
          aptitudes: [
            {
              creatividad:  true
            },
            {
              liderazgo:  false
            },
            {
              seriedad:  true
            }
          ]
        },
        {
          habilidades: [
            {
              trabajoEquipo:  false
            },
            {
              comunicacion:  false
            },
            {
              organizacion:  true
            },
            {
              inovarCrear:  false
            }
          ]
        },
        {
          pasatiempos: [
            {
              futbol:  false
            },
            {
              ejercicio:  true
            },
            {
              natacion:  true
            },
            {
              ciclismo:  false
            },
            {
              atletismo:  true
            },
            {
              musica:  false
            },
            {
              leer:  true
            },
            {
              comer:  false
            }
          ]
        }
      ]
    },
    {
      perfilId: '18',
      apodo: 'Jesy JJ',
      municipio: 'Ixtlahuaca',
      nombre: 'Jessica',
      apellido: 'Julian',
      fotoMin: 'https://fotos.subefotos.com/60e4210e3f85a28216b5dba33df5f087o.jpg',
      fotoPerfil: 'https://fotos.subefotos.com/4e234049c94139d6698973e5e6d5b42do.jpg',
      fotos: [{
        imagen: 'https://fotos.subefotos.com/71b52c684058e4a27a5230ad96a22d4do.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/4e234049c94139d6698973e5e6d5b42do.jpg'
      }
      ],
      descripcion: 'Soy una chica muy buena onda, fiel, detallista, me gusta salir a convivir con amigos',
      meInteresa: '91',
      activo: true,
      status: 'ahora',
      edad: '27 años',
      lat: '19.5715244',
      long: '-99.7715244',
      correo: 'user39@gmail.com',
      cumpleanios: '28/12/1999',
      redes:[
        {
          facebook: false,
          user: 'user/desc'
        },
        {
          tumblr: true,
          user: 'descUser'
        },
        {
          whatsapp: true,
          number: '9345328745'
        },
        {
          twitter: true,
          user: 'user_desc'
        },
        {
          instagram: false,
          user: 'desc_us'
        }
      ],
      acercaDeMi: [
        {
          aptitudes: [
            {
              creatividad:  true
            },
            {
              liderazgo:  false
            },
            {
              seriedad:  true
            }
          ]
        },
        {
          habilidades: [
            {
              trabajoEquipo:  false
            },
            {
              comunicacion:  false
            },
            {
              organizacion:  true
            },
            {
              inovarCrear:  false
            }
          ]
        },
        {
          pasatiempos: [
            {
              futbol:  false
            },
            {
              ejercicio:  true
            },
            {
              natacion:  true
            },
            {
              ciclismo:  false
            },
            {
              atletismo:  true
            },
            {
              musica:  false
            },
            {
              leer:  true
            },
            {
              comer:  false
            }
          ]
        }
      ]
    },
    {
      perfilId: '19',
      apodo: 'Morenita KB',
      municipio: 'Ixtlahuaca',
      nombre: 'Karina',
      apellido: 'Bernal',
      fotoMin: 'https://fotos.subefotos.com/ecfb36d3a541e8f93815b99bc290b719o.jpg',
      fotoPerfil: 'https://fotos.subefotos.com/ecfb36d3a541e8f93815b99bc290b719o.jpg',
      fotos: [{
        imagen: 'https://fotos.subefotos.com/80416236f3fc48d7b0a1f82efeb16198o.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/04a591485d766ef36fbd30be927d67dbo.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/bff0ab256a79ad250225a13df41eef73o.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/160535893a000626b50ec973d5b3f5e6o.jpg'
      }
      ],
      descripcion: 'Soy una chica muy buena onda, fiel, detallista, me gusta salir a convivir con amigos',
      meInteresa: '201',
      activo: false,
      status: 'hace 42min',
      edad: '26 años',
      lat: '19.5715244',
      long: '-99.7715244',
      correo: 'user39@gmail.com',
      cumpleanios: '28/12/1999',
      redes:[
        {
          facebook: false,
          user: 'user/desc'
        },
        {
          tumblr: true,
          user: 'descUser'
        },
        {
          whatsapp: true,
          number: '9345328745'
        },
        {
          twitter: true,
          user: 'user_desc'
        },
        {
          instagram: false,
          user: 'desc_us'
        }
      ],
      acercaDeMi: [
        {
          aptitudes: [
            {
              creatividad:  true
            },
            {
              liderazgo:  false
            },
            {
              seriedad:  true
            }
          ]
        },
        {
          habilidades: [
            {
              trabajoEquipo:  false
            },
            {
              comunicacion:  false
            },
            {
              organizacion:  true
            },
            {
              inovarCrear:  false
            }
          ]
        },
        {
          pasatiempos: [
            {
              futbol:  false
            },
            {
              ejercicio:  true
            },
            {
              natacion:  true
            },
            {
              ciclismo:  false
            },
            {
              atletismo:  true
            },
            {
              musica:  false
            },
            {
              leer:  true
            },
            {
              comer:  false
            }
          ]
        }
      ]
    },
    {
      perfilId: '20',
      apodo: 'Chaparrita PG',
      municipio: 'San Felipe',
      nombre: 'Patricia',
      apellido: 'Gonzales',
      fotoMin: 'https://fotos.subefotos.com/aa97fa64a11d6810b9564517a7bfa19bo.jpg',
      fotoPerfil: 'https://fotos.subefotos.com/aa97fa64a11d6810b9564517a7bfa19bo.jpg',
      fotos: [{
        imagen: 'https://fotos.subefotos.com/38513f6749a7be33c5000f515c21f74do.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/417d0670ed0001b3deaae2493c6fefb7o.jpg'
      }
      ],
      descripcion: 'Soy una chica muy buena onda, fiel, detallista, me gusta salir a convivir con amigos',
      meInteresa: '21',
      activo: false,
      status: 'hace 1min',
      edad: '25 años',
      lat: '19.7142243',
      long: '-99.9592461',
      correo: 'user39@gmail.com',
      cumpleanios: '28/12/1999',
      redes:[
        {
          facebook: false,
          user: 'user/desc'
        },
        {
          tumblr: true,
          user: 'descUser'
        },
        {
          whatsapp: true,
          number: '9345328745'
        },
        {
          twitter: true,
          user: 'user_desc'
        },
        {
          instagram: false,
          user: 'desc_us'
        }
      ],
      acercaDeMi: [
        {
          aptitudes: [
            {
              creatividad:  true
            },
            {
              liderazgo:  false
            },
            {
              seriedad:  true
            }
          ]
        },
        {
          habilidades: [
            {
              trabajoEquipo:  false
            },
            {
              comunicacion:  false
            },
            {
              organizacion:  true
            },
            {
              inovarCrear:  false
            }
          ]
        },
        {
          pasatiempos: [
            {
              futbol:  false
            },
            {
              ejercicio:  true
            },
            {
              natacion:  true
            },
            {
              ciclismo:  false
            },
            {
              atletismo:  true
            },
            {
              musica:  false
            },
            {
              leer:  true
            },
            {
              comer:  false
            }
          ]
        }
      ]
    },
    {
      perfilId: '21',
      apodo: 'Guapa ME',
      municipio: 'Ixtlahuaca',
      nombre: 'Martha',
      apellido: 'Avila',
      fotoMin: 'https://fotos.subefotos.com/10960be950b64fdb06a4a9939da4bbcao.jpg',
      fotoPerfil: 'https://fotos.subefotos.com/10960be950b64fdb06a4a9939da4bbcao.jpg',
      fotos: [{
        imagen: 'https://fotos.subefotos.com/e0d8b96fa5aca1f5f8deffe427cc4104o.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/7a030704e223b2e83aad5f064a978800o.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/9f83d15c3ba5753db5aad50e649c15c6o.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/bf23554f453cf826db6c50d2ca3f1bc6o.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/4d66b1572ce48259f1df238e5bd0467co.jpg'
      }
      ],
      descripcion: 'Soy una chica muy buena onda, fiel, detallista, me gusta salir a convivir con amigos',
      meInteresa: '21',
      activo: true,
      status: 'ahora',
      edad: '28 años',
      lat: '19.5715244',
      long: '-99.7715244',
      correo: 'user39@gmail.com',
      cumpleanios: '28/12/1999',
      redes:[
        {
          facebook: false,
          user: 'user/desc'
        },
        {
          tumblr: true,
          user: 'descUser'
        },
        {
          whatsapp: true,
          number: '9345328745'
        },
        {
          twitter: true,
          user: 'user_desc'
        },
        {
          instagram: false,
          user: 'desc_us'
        }
      ],
      acercaDeMi: []
    },
    {
      perfilId: '22',
      apodo: 'Preciosa YC',
      municipio: 'Ixtlahuaca',
      nombre: 'Yaritza',
      apellido: 'Camilo',
      fotoMin: 'https://fotos.subefotos.com/81c980c8fb8e08e3e799632dc58024d7o.jpg',
      fotoPerfil: 'https://fotos.subefotos.com/81c980c8fb8e08e3e799632dc58024d7o.jpg',
      fotos: [{
        imagen: 'https://fotos.subefotos.com/4b8dfa3c190cbba94530bba295e9c6abo.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/0ed34429361c82e9181ace659dd05b01o.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/bbc6a1364bdbfb88c95d5e89cf054b33o.jpg'
      },
      {
        imagen: 'https://fotos.subefotos.com/62693e0327f1e2cee001aa0b8dee6425o.jpg'
      }
      ],
      descripcion: 'Soy una chica muy buena onda, fiel, detallista, me gusta salir a convivir con amigos',
      meInteresa: '31',
      activo: false,
      status: 'hace 5min',
      edad: '19 años',
      lat: '19.5715244',
      long: '-99.7715244',
      correo: 'user39@gmail.com',
      cumpleanios: '28/12/1999',
      redes:[],
      acercaDeMi: [
        {
          aptitudes: [
            {
              creatividad:  true
            },
            {
              liderazgo:  false
            },
            {
              seriedad:  true
            }
          ]
        },
        {
          habilidades: [
            {
              trabajoEquipo:  false
            },
            {
              comunicacion:  false
            },
            {
              organizacion:  true
            },
            {
              inovarCrear:  false
            }
          ]
        },
        {
          pasatiempos: [
            {
              futbol:  false
            },
            {
              ejercicio:  true
            },
            {
              natacion:  true
            },
            {
              ciclismo:  false
            },
            {
              atletismo:  true
            },
            {
              musica:  false
            },
            {
              leer:  true
            },
            {
              comer:  false
            }
          ]
        }
      ]
    }
  ];
  estados = [
    "AGUASCALIENTES",
    "BAJA CALIFORNIA",
    "BAJA CALIFORNIA SUR",
    "CHIHUAHUA",
    "CHIAPAS",
    "CAMPECHE",
    "CIUDAD DE MEXICO",
    "COAHUILA",
    "COLIMA",
    "DURANGO",
    "GUERRERO",
    "GUANAJUATO",
    "HIDALGO",
    "JALISCO",
    "MICHOACAN",
    "ESTADO DE MEXICO",
    "MORELOS",
    "NAYARIT",
    "NUEVO LEON",
    "OAXACA",
    "PUEBLA",
    "QUINTANA ROO",
    "QUERETARO",
    "SINALOA",
    "SAN LUIS POTOSI",
    "SONORA",
    "TABASCO",
    "TLAXCALA",
    "TAMAULIPAS",
    "VERACRUZ",
    "YUCATAN",
    "ZACATECAS"
  ];
  authState = new BehaviorSubject(false);
  constructor(private http: HttpClient, private storage: Storage,
    private navCtrl: NavController,
    private platform: Platform) {
    this.platform.ready().then(() => {
      this.ifLoggedIn();
    });
  }


  userAll(){
    const dir = 'usersAll';
    return this.http.get(dir)
    .subscribe(response => {
      this.users = response;
      console.log("Datos: ", response);
    })
  }

  userId(): Observable<any> {
    const user = {
      idUser: 1
    }
    const dir = this.userById;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    const json = JSON.stringify(user);
    return this.http.post(dir, json, httpOptions);
  }



  ifLoggedIn() {
    this.storage.get('USER_INFO').then((response) => {
      if (response) {
        this.authState.next(true);
      }
    });
  }

  response_login = {
    nombre: '',
    apellido: '',
    correo: '',
    password: ''
  }
  login(val) {
    var service_registro = {
      nombre: val.nombre,
      apellido: val.apellido,
      correo: val.correo,
      password: val.password
    }
    this.storage.set('USER_INFO', service_registro).then((response) => {
      this.response_login = response;
      console.log("Response log: ", this.response_login);
      localStorage.setItem('registro',JSON.stringify(this.response_login));
      this.navCtrl.navigateForward('/folder/Inbox');
      this.authState.next(true);
    });
  }
  

  signUp(){
    this.navCtrl.navigateForward('/folder/Inbox');
    this.authState.next(true);
  }

  logout() {
    this.storage.remove('USER_INFO').then(() => {
      this.navCtrl.navigateForward('/registro');
      this.authState.next(false);
    });
  }

}
