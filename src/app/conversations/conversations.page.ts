import { Component, OnInit } from '@angular/core';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { MenuController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-conversations',
  templateUrl: './conversations.page.html',
  styleUrls: ['./conversations.page.scss'],
})
export class ConversationsPage implements OnInit {

  conversation = [
    {
      fotoMin: 'https://fotos.subefotos.com/95da47dc02e21fc067f545004b28c836o.jpg',
      apodo: 'Cariñoso',
      activo: true,
    },
    {
      fotoMin: 'https://fotos.subefotos.com/0b1c4eb501e22315144e2b727d3b0a53o.jpg',
      apodo: 'Mamasita',
      activo: false,
    },
    {
      fotoMin: 'https://fotos.subefotos.com/0b1c4eb501e22315144e2b727d3b0a53o.jpg',
      apodo: 'Mamasita',
      activo: false,
    },
    {
      fotoMin: 'https://fotos.subefotos.com/f6186afdc344312842a40ec7d05ff233o.jpg',
      apodo: 'Bonita MRG',
      activo: true,
    },
    {
      fotoMin: 'https://fotos.subefotos.com/26019ccec32c7b72d8f213d64eb0a238o.jpg',
      apodo: 'Sexy AS',
      activo: false,
    },
    {
      fotoMin: 'https://fotos.subefotos.com/f6186afdc344312842a40ec7d05ff233o.jpg',
      apodo: 'Bonita MRG',
      activo: true,
    },
    {
      fotoMin: 'https://fotos.subefotos.com/0b1c4eb501e22315144e2b727d3b0a53o.jpg',
      apodo: 'Mamasita',
      activo: false,
    },
    {
      fotoMin: 'https://fotos.subefotos.com/f6186afdc344312842a40ec7d05ff233o.jpg',
      apodo: 'Bonita MRG',
      activo: true,
    },
    {
      fotoMin: 'https://fotos.subefotos.com/26019ccec32c7b72d8f213d64eb0a238o.jpg',
      apodo: 'Sexy AS',
      activo: false,
    },
    {
      fotoMin: 'https://fotos.subefotos.com/26019ccec32c7b72d8f213d64eb0a238o.jpg',
      apodo: 'Sexy AS',
      activo: false,
    },
    {
      fotoMin: 'https://fotos.subefotos.com/e0e33ab4b0ed1fd07216619a2b9483afo.jpg',
      apodo: 'Preciosa AG',
      activo: false,
    }
  ]

  constructor(private navCtrl: NavController, private screen: ScreenOrientation, private menu: MenuController) {
    this.screen.lock(this.screen.ORIENTATIONS.PORTRAIT).catch(function(error) {}); this.screen.onChange().subscribe(() => { });
    this.menu.enable(false);
   }

  ngOnInit() {
  }

  back(){
    this.menu.enable(true);
    setTimeout(() => {
      this.navCtrl.navigateForward('/folder/Inbox');
    }, 500);
  }

}
