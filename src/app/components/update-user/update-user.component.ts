import { Component, OnInit, Input, Output } from '@angular/core';
import { ModalController, NavParams, NavController } from '@ionic/angular';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.scss'],
})
export class UpdateUserComponent implements OnInit {
  @Input() public lunch: string;
  @Output() public recivo_foto: string;
  public dinner = {
    mainCourse: 'fried chicken',
    desert: 'chocolate cake'
  };
  foto;
  apodo;
  public navParams = new NavParams();
  upd_nombre;
  upd_correo;
  upd_password;
  conf_password;
  nombre;
  correo;
  password;

  public nombre_modal:boolean = false;
  public correo_modal:boolean = false;
  public password_modal:boolean = false;
  i = true;

  constructor(private modalController: ModalController, public nav: NavController) {
   }

  ngOnInit() {}
  mostrarNombre(){
    this.nombre_modal = true;
    this.correo_modal = false;
  }
  mostrarCorreo(){
    this.correo_modal = true;
    this.nombre_modal = false;
    console.log("correo");
  }
  mostrarPassword(){
    this.password_modal = true;
  }

  async closeModal() {
    await this.modalController.dismiss();
  }

  updateNombre(){
    this.nombre = this.upd_nombre;
    console.log(": ",this.nombre);
    this.closeModal();
  }
  correoinvalido;
  updateCorreo(){
    this.correo = this.upd_correo;
    console.log(": ",this.correo);
    this.closeModal();
  }
  expresion = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
  passwordinvalido = false;
  updatePassword(){
    if(this.upd_password === this.conf_password){
      this.passwordinvalido = true;
      setTimeout(() => {
        this.passwordinvalido = false;
      }, 1300);
      console.log("No coinciden");
    }else{
      this.password = this.upd_password;
      console.log("Pas: ",this.password);
     // this.closeModal();
    }
  }
}
