import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-update-photo',
  templateUrl: './update-photo.component.html',
  styleUrls: ['./update-photo.component.scss'],
})
export class UpdatePhotoComponent implements OnInit {
  opcion = [
    {
      id_opcion: '1',
      opcion_: 'Tomar foto'
    },
    {
      id_opcion: '2',
      opcion_: 'Seleccionar foto'
    },
    {
      id_opcion: '3',
      opcion_: 'Eliminar foto'
    }
  ]
  constructor(private popoverController: PopoverController) { }

  ngOnInit() {}
  onClick(val) {
    console.log("Valor:", val);
    this.popoverController.dismiss({
      op: val
    }).catch(function(error) {
      console.log("ERR: ",error);
     })
  }
}
