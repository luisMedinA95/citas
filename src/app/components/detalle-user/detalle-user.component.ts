import { Component, OnInit } from '@angular/core';
import { ModalController, NavController, NavParams, ToastController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { File } from '@ionic-native/file/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';


@Component({
  selector: 'app-detalle-user',
  templateUrl: './detalle-user.component.html',
  styleUrls: ['./detalle-user.component.scss'],
})
export class DetalleUserComponent implements OnInit {
  foto;
  apodo;
  public navParams = new NavParams();
  nombre_modal;
  constructor(private modalController: ModalController, public nav: NavController, private http: HttpClient, private file: File, 
    private transfer: FileTransfer, private toastController: ToastController ) { }
    
  ngOnInit() { }
  async closeModal() {
    await this.modalController.dismiss();
  }
  cargarFoto(val, apo) {
    this.foto = val;
    this.apodo = apo;
  }

  savePhoto(val) {
    console.log("Foto: ", val);
    const icon = document.getElementById('icon_download');
    icon.style.color = "#AFB3CF";
    const fileTransfer: FileTransferObject = this.transfer.create();
    fileTransfer.download(val, this.file.externalDataDirectory + 'foto.jpg').then((entry) => {
      //console.log(entry.toURL());
      this.showToast();
      setTimeout(() => {
        icon.style.color = "white";
      }, 1100);
    }, (error) => {
      console.log('Err: ',error);
    });
  }


  myToast: any;
  showToast() {
    this.myToast = this.toastController.create({
      message: 'Guardado en el dispositivo',
      position: 'bottom',
      duration: 1350
    }).then((toastData) => {
      console.log(toastData);
      toastData.present();
    });
  }
}
