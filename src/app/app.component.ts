import { Component, OnInit, Input } from '@angular/core';
import { Platform, NavController, LoadingController, MenuController, PopoverController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { ServicioService } from './services/servicio.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { UpdatePhotoComponent } from './components/update-photo/update-photo.component';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  @Input() texto = 'Cargando...';
  loader = false;
  public Icns = [
    {
      title: 'Inicio',
      url: '/folder/Inbox',
      icon: 'home'
    }
  ];
  usuario;
  correo;
  Lat;
  Long;
  ub;
  constructor(private platform: Platform, private splashScreen: SplashScreen, private service: ServicioService, private menu: MenuController, private popoverController: PopoverController,
    private statusBar: StatusBar, private screen: ScreenOrientation, private navCtrl: NavController, private loadingController: LoadingController, private geo: Geolocation) {
    this.screen.lock(this.screen.ORIENTATIONS.PORTRAIT).catch(function (error) { }); this.screen.onChange().subscribe(() => { });
    this.initializeApp();
    this.consultarUsuario();
    this.miUbicacion();
    /*if (localStorage.getItem('inicio_registro') === '1' || localStorage.getItem('inicio_login') === '1') {
      this.navCtrl.navigateForward('/folder/Inbox');
    } else {
      this.navCtrl.navigateForward('/registro');
    }*/
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.show();
      this.splashScreen.hide();
      this.checkDarkTheme();
    });
  }
  ngOnInit() {
  }

  miUbicacion() {
    this.geo.getCurrentPosition().then((resp) => {
      this.Lat = resp.coords.latitude
      this.Long = resp.coords.longitude
      this.ub = this.Lat + "," + this.Long;
      localStorage.setItem('mi_ubic', this.ub);
      console.log(localStorage.getItem('mi_ubic'));
    }).catch((error) => {
      console.log('Ubicacion no encontrda', error);
    });

    let watch = this.geo.watchPosition();
    watch.subscribe((data) => {
    });
  }

  consultarUsuario() {
    this.usuario = this.service.response_login.nombre + " " + this.service.response_login.apellido;
    this.correo = this.service.response_login.correo;
  }

  checkDarkTheme() {
    const prefersDark = window.matchMedia('(prefers-color-scheme: dark)');
    if (prefersDark.matches) {
      document.body.classList.toggle('dark');
    }
  }

  darkMode: Boolean = true;
  cambio() {
    this.darkMode = !this.darkMode;
    document.body.classList.toggle('dark');
  }

  async opciones(ev) {
    const popover = await this.popoverController.create({
      component: UpdatePhotoComponent,
      event: ev,
    });
    await popover.present();
    const { data } = await popover.onDidDismiss();
    if (data === undefined) {
     
    } else {
      if (data.op.id_opcion === '2') {
      } else if (data.op.id_opcion === '3') {
      }
    }
    console.log("Recivo: ", data);
  }

  salir() {
    this.loading();
    setTimeout(() => {
      localStorage.removeItem('inicio_registro');
      localStorage.removeItem('inicio_login');
      this.navCtrl.navigateForward('/registro');
      this.menu.enable(false);
    }, 1000);
  }

  inicio() {
    this.loading();
    setTimeout(() => {
      this.navCtrl.navigateForward('/folder/Inbox');
    }, 2500);
  }
  favoritos() {
    this.loading();
    setTimeout(() => {
      this.navCtrl.navigateForward('/favoritos');
    }, 2500);
  }

  configuracion() {
    this.loading();
    setTimeout(() => {
      this.navCtrl.navigateForward('/configuracion');
    }, 2500);
  }

  chats(){
    this.loading();
    setTimeout(() => {
      this.navCtrl.navigateForward('/conversations');
    }, 2500);
  }

  isLoading = false;
  async loading() {
    this.isLoading = true;
    return await this.loadingController.create({
      message: 'Cargando',
      duration: 1000
    }).then(a => {
      a.present().then(() => {
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort laoding'));
        }
      });
    });
  }
}
