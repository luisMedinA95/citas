import { Component, OnInit, Input } from '@angular/core';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { MenuController, NavController, LoadingController } from '@ionic/angular';
import swal from 'sweetalert';
@Component({
  selector: 'app-favoritos',
  templateUrl: './favoritos.page.html',
  styleUrls: ['./favoritos.page.scss'],
})
export class FavoritosPage implements OnInit {
  @Input() texto = 'Cargando...';
  loader = false;
  favorito;
  valor;
  sin_favoritos = false;

  constructor(private screen: ScreenOrientation, private menu: MenuController, private navCtrl: NavController,
    private loadingController: LoadingController) {
    this.screen.lock(this.screen.ORIENTATIONS.PORTRAIT).catch(function(error) {}); this.screen.onChange().subscribe(() => { });
    this.menu.enable(false);
    this.resultados();
  }

  ngOnInit() {
  }

  visitar_perfil(val) {
    console.log("-->: ", val);
    this.loading();
    setTimeout(() => {
      let act_resultado = '2';
      localStorage.setItem('favorito_perfil', JSON.stringify(val));
      localStorage.setItem('favorito_user', act_resultado);
      this.navCtrl.navigateForward("/perfil");
    }, 1500);
  }
  isLoading = false;
  async loading() {
    this.isLoading = true;
    return await this.loadingController.create({
      message: 'Cargando',
      duration: 2000
    }).then(a => {
      a.present().then(() => {
        console.log('loading presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort laoding'));
        }
      });
    });
  }

  elemento(p) {
    swal({
      text: "¿Estas seguro de eliminar de favoritos?",
      icon: "warning",
      dangerMode: true,
    })
      .then(eliminado => {
        if (eliminado) {
          var index = this.favorito.indexOf(p);
          if (index > -1) {
            this.favorito.splice(index, 1);
            localStorage.setItem('val_favoritos', JSON.stringify(this.favorito));
            swal(p.apodo, "Eliminado correctamente de favoritos", "success");
            if (Object.entries(this.favorito).length === 0) {
              console.log("VACIO")
              this.sin_favoritos = true;
              let ident = '1';
              localStorage.setItem('Identif', ident);
            } else {
              this.sin_favoritos = false;
            }
          }
        }
      });
  }

  resultados() {
    this.valor = localStorage.getItem('val_favoritos');
    this.favorito = JSON.parse(this.valor);
    const activo = localStorage.getItem('Identif');
    if (this.favorito === null || activo === '1') {
      this.sin_favoritos = true;
      if (activo === '1' && this.favorito.indexOf(0)) {
        this.sin_favoritos = false;
        if (Object.entries(this.favorito).length === 0 && activo === '1') {
          this.sin_favoritos = true;
        }
      }
    } else {
      this.sin_favoritos = false;
    }
  }

  back() {
    this.menu.enable(true);
    this.loader = true;
    setTimeout(() => {
      this.loader = false;
      this.texto = 'Cargando...';
      this.navCtrl.navigateForward('/folder/Inbox');
    }, 1000);
  }

}
